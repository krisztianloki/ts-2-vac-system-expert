<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>${GAUGE} DAQ</name>
  <width>210</width>
  <height>300</height>
  <widget type="rectangle" version="2.0.0">
    <name>Background</name>
    <class>GROUP</class>
    <width>210</width>
    <height>300</height>
    <line_width>2</line_width>
    <line_color use_class="true">
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color use_class="true">
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <text>  ${GAUGE}</text>
    <width>210</width>
    <height>30</height>
    <font>
      <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Voltage</name>
    <text>Voltage:</text>
    <x>10</x>
    <y>45</y>
    <width>74</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>PrsRawR</name>
    <pv_name>${GAUGE}:PrsRawR</pv_name>
    <x>90</x>
    <y>45</y>
    <width>110</width>
    <height>25</height>
    <precision>7</precision>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DAQErr-Alrm</name>
    <pv_name>${GAUGE}:DAQErr-Alrm</pv_name>
    <x>20</x>
    <y>80</y>
    <width>50</width>
    <height>25</height>
    <off_label>Error</off_label>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_label>Error</on_label>
    <on_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </on_color>
    <square>true</square>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DAQUnderLim-Alrm</name>
    <pv_name>${GAUGE}:DAQUnderLim-Alrm</pv_name>
    <x>80</x>
    <y>80</y>
    <width>50</width>
    <height>25</height>
    <off_label>Under</off_label>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_label>Under</on_label>
    <on_color>
      <color red="40" green="140" blue="40">
      </color>
    </on_color>
    <square>true</square>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DAQOverLim-Alrm</name>
    <pv_name>${GAUGE}:DAQOverLim-Alrm</pv_name>
    <x>140</x>
    <y>80</y>
    <width>50</width>
    <height>25</height>
    <off_label>Over</off_label>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_label>Over</on_label>
    <on_color>
      <color red="252" green="242" blue="17">
      </color>
    </on_color>
    <square>true</square>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Relay</name>
    <text>Relay:</text>
    <x>10</x>
    <y>115</y>
    <width>74</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="Tooltip" prop_id="tooltip" out_exp="true">
        <exp bool_exp="True">
          <expression>pvStr0</expression>
        </exp>
        <pv_name>${GAUGE}:DAQRlySel-RB</pv_name>
      </rule>
    </rules>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>RlyDAQ_StatR</name>
    <pv_name>${GAUGE}:RlyDAQ_StatR</pv_name>
    <x>90</x>
    <y>115</y>
    <width>110</width>
    <height>25</height>
    <off_color>
      <color name="LED-BLUE-ON" red="81" green="232" blue="255">
      </color>
    </off_color>
    <on_color>
      <color name="LED-BLUE-OFF" red="90" green="110" blue="110">
      </color>
    </on_color>
    <square>true</square>
    <labels_from_pv>true</labels_from_pv>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Buffer</name>
    <text>Buffer:</text>
    <x>10</x>
    <y>150</y>
    <width>74</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="led" version="2.0.0">
    <name>DAQPMSavingR</name>
    <pv_name>${GAUGE}:DAQPMStatR</pv_name>
    <x>90</x>
    <y>150</y>
    <width>110</width>
    <height>25</height>
    <off_color>
      <color name="LED-BLUE-OFF" red="90" green="110" blue="110">
      </color>
    </off_color>
    <on_color>
      <color name="LED-BLUE-ON" red="81" green="232" blue="255">
      </color>
    </on_color>
    <square>true</square>
    <labels_from_pv>true</labels_from_pv>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Trigger on</name>
    <text>Trigger on:</text>
    <x>10</x>
    <y>185</y>
    <width>74</width>
    <height>25</height>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>DAQRlySelS</name>
    <pv_name>${GAUGE}:DAQRlySelS</pv_name>
    <x>90</x>
    <y>185</y>
    <width>110</width>
    <height>25</height>
    <show_confirm_dialog>true</show_confirm_dialog>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Manual Trigger</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Manual Trigger</description>
      </action>
    </actions>
    <pv_name>${GAUGE}:DAQManuTrigCmd</pv_name>
    <x>10</x>
    <y>220</y>
    <width>190</width>
    <tooltip>$(actions)</tooltip>
    <show_confirm_dialog>true</show_confirm_dialog>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>../../faceplates/pmbuffer.bob</file>
        <target>window</target>
        <description>Show Latest Buffer</description>
      </action>
      <action type="open_display">
        <file>../../faceplates/archived_pmbuffer.bob</file>
        <target>window</target>
        <description>Show Archived Buffer</description>
      </action>
    </actions>
    <text>Show Buffer</text>
    <x>10</x>
    <y>260</y>
    <width>190</width>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
